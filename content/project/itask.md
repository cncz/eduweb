+++
date = "2016-12-14T11:24:43+01:00"
name = "iTask"
title = "itask"
summary = "Dynamic Workflow Management"
web = "http://wiki.clean.cs.ru.nl/ITasks"
download_link = "http://wiki.clean.cs.ru.nl/Download_Clean"
demolink = ""
repo = "https://gitlab.science.ru.nl/clean-and-itasks/homebrew-clean-and-itasks.git"
forum_link = ""
link_to_docs = "http://wiki.clean.cs.ru.nl/Publications"
contact = "rinus@cs.ru.nl"
cardthumbimage = "/images/itask.jpg"
cardheaderimage = "/images/itask-header.jpg"
image = "Itasks-logo.png"
sponsors = ["stitpro"]
tags = ["stitpro"]
comment = false

[author]
  description = "Professor at CS"
  email = "rinus@cs.ru.nl"
  github = ""
  name = "Rinus Plasmeijer"
  twitter = ""
  website = ""
  department = "Computer Science"
  institute = "Radboud University"

+++

# iTask

## Interactive Dynamic Workflow Management System

Target group: Students and researchers from Universities, R & D departments
in businesses. Developers Model Based Software Development Department,
Institute for Computing and Information Sciences, Radboud 
University (specifically Jeroen Henrix and Steffen Michels). Start Date
01-01-2011 Scheduled completion date 01-5-2011 Website
http://wiki.clean.cs.ru.nl/ITasks Contact project: Professor Rinus Plasmeijer,
tel 024-3652644, email:. Rinus@cs.ru.nl

## Project description

## iTasks – Task Oriented Programming

The iTasks framework is an Domain Specific Language embedded in the functional
language Clean for developing multi-user, web-enabled, client-server
applications. It offers a special flavour of functional programming,
Task-Oriented Programming (TOP), where the notion of tasks plays the central
role. In iTasks one specifies the tasks that end-users and systems have to do
to accomplish a certain goal, on a very high-level of abstraction. Programmers
do not have to worry about all kinds of implementation details that commonly
play a role when distributed applications have to be developed, such as
communication, synchronization, data storage and retrieval, data persistence,
user-interface creation and handling.  From a specification in one source
language, Clean, a web-server is generated which coordinates the work thus
described. To provide a specific end-user client with a user-interface for
doing the task, web-pages are dynamically generated for the tasks to be done
(JavaScript), that can be inspected by any HTML 5 compatible browser. In this
way, any PC, tablet or phone can be used by the end-user to do his or her work.

The first paper on iTasks has been published in 2007 at the ICFP conference,
and over 30 papers has been published since then. The research was triggered by
industrial users who found existing commercial workflow systems too rigid. The
research on Task Oriented Programming is conducted by prof. Rinus Plasmeijer
and dr. Bas Lijnse at the Radboud University Nijmegen, in collaboration with
the NLDA (Nederlandse Defensie Academie, dr. Jan Martin Jansen) and the
University of Utrecht (prof. Johan Jeuring). The research is funded by STW, the
Ministry of Defence (Navy), TNO, the Dutch Coastguard, and the Dutch Tax
Office. 


### status

The iTask system runs on any mixture of Intel (Windows 10, Mac) and ARM
processors (Android, Raspberry Pi), generates JavaScript pages with which one
can interact with any HTML5 compatible browser. One can download the system
from http://wiki.clean.cs.ru.nl/Download_Clean The system is currently mainly
used for rapid prototyping. But it can also be used to formalize the way people
work or should work, to analyse and simulate ways of working, to educate and
train people, and of course to construct distributed multi-user applications.


## Sub-project 1 (Henrix)

The iTask system is flexible and powerful, but has as  disadvantage that the
source code is difficult to read for non-programmers, such as domain experts
and managers. They are more used to graphic representations such as flow
charts. Henrix has designed a Graphical Notation for iTasks (Gin), and a
prototype implementation. A Gin flow chart is translated into Clean source
code. If the flow is correct, the translation is converted to an executable
workflow which can be dynamically added to the running system.  

Gin is a proof-of-concept implementation with some restrictions (among others,
one cannot define new types) as described in the paper (see paper 22.). 

### Status Update 13-12-2016

With the iTask system indeed arbitrary complex ways of working can be described
making using of arbitrary platforms and devices, varying from PC, laptop,
tablet, smart phone to the Internet of Things. We are quite happy with the
system. However, it is still not easy enough for non-programmers to turn their
knowledge how work should be done into an executable iTask specification
without help of iTask programmers. The input from domain experts is vital,
because only in this way one can obtain a first version of a system which
supports the way work takes place. From an iTask specification we can generate
a flow chart (blueprint) that we can show to domain experts to see if the
application is indeed according to their wish (see paper 6.). The blueprint can
be inspected at run-time where it shows how the work is progressing. However,
we still want need the Gin way of working as well, but the prototype made by
Jeroen Henrix was still too complicated to use for domain experts. We are
currently designing an improved version of Gin that should allow users with
different skills (managers, domain experts, programmers) to work on a
specification on different levels of abstraction, at the same time, in any
order.


## [Publications about iTasks](http://wiki.clean.cs.ru.nl/Publications).

1. Peter Achten, Pieter Koopman, Rinus Plasmeijer. An Introduction to Task Oriented Programming. In Zsók, V., Horváth, Z., Csató, L. Eds. Central European Functional Programming School, Revised Selected Papers, 5th Summer School, CEFP 2013, Cluj-Napoca, Romania, Lecture Notes in Computer Science, vol. 8606, pp. 187-245, http://dx.doi.org/10.1007/978-3-319-15940-9_5. 

2. Peter Achten, Jurriën Stutterheim, László Domoszlai, Rinus Plasmeijer. Task Oriented Programming with Purely Compositional Interactive Scalable Vector Graphics. In Tobin-Hochstadt, S. Ed. Proceedings 26th International Symposium Implementation and Application of Functional Languages, IFL 2014, Boston, MA, USA, October 1-3, 2014, ACM Digital Library. 

3. László Domoszlai, Bas Lijnse, Rinus Plasmeijer. Editlets: type based client side editors for iTasks. In Tobin-Hochstadt, Sam., Ed. Proceedings 26th International Workshop on the Implementation of Functional Languages, IFL '14, Boston, U.S.A., ACM Digital Library, Oct 1-3 2014. 

4. László Domoszlai, Bas Lijnse, Rinus Plasmeijer. Parametric Lenses: change notification for bidirectional lenses. In Tobin-Hochstadt, Sam., Ed. Proceedings 26th International Workshop on the Implementation of Functional Languages, IFL '14, Boston, U.S.A., ACM Digital Library, Oct 1-3 2014. 

5. Jurriaan van Diggelen, Wilfried Post, Marleen Rakhorst, Rinus Plasmeijer, Wessel van Staal. Using Process-Oriented Interfaces for Solving the Automation Paradox in Highly Automated Navy Vessels. In Proceedings of the 10th International Conference on Active Media Technology, AMT 2014, Warsaw, Poland, August 11-14, 2014, Springer, 2014, pp. 442-452.

6. Jurrien Stutterheim, Rinus Plasmeijer, Peter Achten. Tonic: An Infrastructure to Graphically Represent the Definition and Behaviour of Tasks. In Hage, Jurriaan and McCarthy, Jay, Eds. Trends in Functional Programming, Springer Berlin Heidelberg, LNCS 8843, 2014, DOI:10.1007/978-3-319-14675-1_8, pp. 122 - 141. 

7. M. van der Heijden, P.J.F. Lucas, B. Lijnse, Y.F. Heijdra, T.R.J. Schermer. An autonomous mobile system for the management of COPD. In: Journal of Biomedical Informatics, 46 (3), 2013, pp. 458-469. 

8. Lijnse, Bas. Evolution of a Parallel Task Combinator. In Achten, P. and Koopman, P. Eds. The Beauty of Functional Code - Essays Dedicated to Rinus Plasmeijer on the Occasion of His 61st Birthday, Festschrift, LNAI 8106, August 2013, ISBN 978-3-642-40354-5, pp. 193-210. 

9. Lijnse, Bas. TOP to the Rescue - Task-Oriented Programming for Incident Response Applications. PhD Thesis. Institute for Computing and Information Sciences, Radboud University Nijmegen, The Netherlands, 2013. 

10. Jeroen Henrix, Rinus Plasmeijer, Peter Achten. GiN: a graphical language and tool for defining iTask workflows. In Ricardo Peña, Rex Page, Eds. Proceedings of the 12th Symposium on Trends in Functional Programming, TFP '11, Revised Selected Papers, Ricardo Peña, Madrid, Spain, May 2011, Springer, LNCS 7193. 

11. Bas Lijnse, Jan Martin Jansen and Rinus Plasmeijer. Incidone: A Task-Oriented Incident Coordination Tool. Proceedings of the 9th International Conference on Information Systems for Crisis Response and Management, ISCRAM '12, Leon Rothkrantz, Jozef Ristvej and Zeno Franco, Vancouver, Canada, 2012, April. 

12. Steffen Michels and Rinus Plasmeijer. Uniform data sources in a functional language. Presented at Symposium on Trends in Functional Programming, TFP ’12. 

13. Rinus Plasmeijer, Peter Achten, Bas Lijnse and Steffen Michels. Defining multi-user web applications with iTasks. Proceedings of the 4th Central European Functional Programming School, CEFP '11, Viktória Zsók, Zoltán Horváth and Rinus Plasmeijer, 2012, 14-24, June, Eötvös Loránd University, Budapest, Hungary, Springer, LNCS, 7241, pp.46-92. 

14. Rinus Plasmeijer, Bas Lijnse, Steffen Michels, Peter Achten and Pieter Koopman. Task-Oriented Programming in a Pure Functional Language. In Proceedings of the 2012 ACM SIGPLAN International Conference on Principles and Practice of Declarative Programming, PPDP'12, September 2012, Leuven, Belgium, ACM, ISBN 978-1-4503-1522-7, pp.195-206.

15. Maarten van der Heijden, Bas Lijnse, Peter Lucas, Yvonne Heijdra, Tjard Schermer. Managing COPD exacerberations with telemedicine. In 13th Conference on Artificial Intelligence in Medicine, AIME, july 2011, Bled, Slovenia, Springer-Verlag, LNCS 6747, pp. 169-178. 

16. Jan Martin Jansen, Rinus Plasmeijer, Pieter Koopman. iEditors: Extending iTask with Interactive Plug-ins. In Scholz, S-B., Chitil, O. Eds. Selected Papers Proceedings 20th International Symposium on the Implementation and Application of Functional Languages, University of Hertfordshire, United Kingdom, 10-12 September, 2008, Springer, LNCS 5836, pp. 192-211. 

17. Pieter Koopman, Rinus Plasmeijer, Peter Achten. An Executable and Testable Semantics for iTasks. In Scholz, S-B., Chitil, O. Eds. Selected Papers Proceedings 20th International Symposium on the Implementation and Application of Functional Languages, University of Hertfordshire, United Kingdom, 10-12 September, 2008, Springer, LNCS 5836, pp. 212-232.

18. Bas Lijnse, Jan Martin Jansen, Ruud Nanne, Rinus Plasmeijer. Capturing the Netherlands Coast Guard's SAR Workflow with iTasks. Proceedings of the 8th International Conference on Information Systems for Crisis Response and Management, ISCRAM'11, ISCRAM Association, David Mendonca and Julie Dugdale, Lisbon, Portugal, 2011, May, Awarded the Mike Meleshkin Award for Best Ph.D. Student Paper. 

19. Steffen Michels, Rinus Plasmeijer, Peter Achten. iTask as a new paradigm for building GUI applications. In Jurriaan Hage, Ed. Proceedings of the 22nd International Symposium on the Implementation and Application of Functional Languages, IFL'10, Selected Papers, Alphen aan den Rijn, The Netherlands, 2011, Springer, LNCS, 6647, pp. 153-168. 

20. Rinus Plasmeijer, Peter Achten, Pieter Koopman, Bas Lijnse, Thomas van Noort, John van Groningen. iTasks for a change - Type-safe run-time change in dynamically evolving workflows. proc. of the 20th International Workshop on Partial Evaluation and Program Manipulation, PEPM '11, Austin, TX, USA, ACM, ISBN 978-1-4503-0485-6, pp. 151 - 160. 

21. Rinus Plasmeijer, Bas Lijnse, Peter Achten, Steffen Michels. Getting a Grip on Tasks that Coordinate Tasks. Invited Paper. proc. of the 11th International Workshop on Language Descriptions, Tools and Applications, LDTA '11, Saarbrucken, ACM, pp. 1 - 7 

22. Jeroen Henrix, Rinus Plasmeijer, Peter Achten. Gin: Graphical iTask Notation [extended abstract]. In Jurriaan Hage, Ed. Preproceedings of the 22nd Symposium on Implementation and Application of Functional Languages, IFL 2010, September 1-3 2010, Alphen aan den Rijn. Technical Report UU-CS-2010-020, ISSN: 0924-3275, Utrecht University, Utrecht, the Netherlands. 

23. Jan Martin Jansen, Rinus Plasmeijer, Pieter Koopman, Peter Achten. Embedding a Web-Based Workflow Management System in a Functional Language - Experience paper. In Claus Brabrand, Pierre-Etienne Moreau, Eds. Proceedings 10th Workshop on Language Descriptions, Tools and Applications, LDTA 2010, Paphos, Cyprus, March 27-28, 2010, pp. 79-93. 

24. Jan Martin Jansen, Bas Lijnse, Rinus Plasmeijer. Towards Dynamic Workflows for Crisis Management. In Simon French, Brain Tomaszewski, Cristopher Zobel, Eds. Proceedings of the 7th International Conference on Information Systems for Crisis Response and Management, ISCRAM'10, Seattle, WA, USA, may 2010. 

25. Jan Martin Jansen, Bas Lijnse, Rinus Plasmeijer. Web Based Dynamic Workflow Systems for C2 of Military Operations. In Revised Selected Papers of the 15th ICCRTS, Santa Monica, California, June 22-24, 2010. 

26. Bas Lijnse, Rinus Plasmeijer. iTasks 2: iTasks for End-users. In Marco T. Morazán, Sven-Bodo Scholz. Eds. Revised Selected Papers of the 21st Symposium on Implementation and Application of Functional Languages, September 23-25, 2009, Seton Hall University, South Orange, NJ, USA. Springer-Verlag, LNCS 6041, pp.36-54. 

27. Steffen Michels, Rinus Plasmeijer, Peter Achten. iTask as a new paradigm to building GUI applications [extended abstract]. In Jurriaan Hage, Ed. Preproceedings of the 22nd Symposium on Implementation and Application of Functional Languages, IFL 2010, September 1-3 2010, Alphen aan den Rijn. Technical Report UU-CS-2010-020, ISSN: 0924-3275, Utrecht University, Utrecht, the Netherlands. 

28. Rinus Plasmeijer, Peter Achten, Pieter Koopman, Bas Lijnse, Thomas van Noort. An iTask case study: a conference management system. In Koopman, P., Plasmeijer, R., Swierstra, D. Eds. Advanced Functional Programming, 6th International School, AFP 2008, Revised Lectures, Center Parcs "Het Heijderbos", The Netherlands, May 19-24 2008, LNCS 5832, Springer, pp. 306-329. 

29. Jan Martin Jansen, Pieter Koopman, Rinus Plasmeijer. Web based Dynamic Workflow Systems and Applications in the Military Domain. In Hupkens, Th. and Monsuur, H. Eds. NL ARMS, Netherlands Annual Review of Military Studies 2008, Sensors, Weapons, C4I and Operations Research, ISSN: 0166-9982, pp. 43-59. bib 

30. Jan Martin Jansen, Rinus Plasmeijer, Pieter Koopman. iEditors: Extending iTask with Interactive Plug-ins. In Scholz, S-B. Ed. Proceedings Implementation and Application of Functional Languages, 20th International Symposium, IFL 2008, Hatfield, Hertfordshire, UK, 10-12 September 2008, Technical Report No. 474, September 2008, University of Hertfordshire, pp. 170-186. abstract bib 

31. Rinus Plasmeijer, Peter Achten, Pieter Koopman. An Introduction to iTasks: Defining Interactive Work Flows for the Web. In Horváth, Plasmeijer, Soós, Zsók, Eds. 2nd Central European Functional Programming School, CEFP 2007, June 23-30 2007, Cluj-Napoca, Romania. Springer LNCS 5161, pp.1-40. abstract bib Springer-link 

32. Rinus Plasmeijer, Peter Achten, Pieter Koopman, Bas Lijnse, Thomas van Noort. Specifying Interactive Work Flows for the Web. In Koopman, P., Plasmeijer, R., Swierstra, D. Eds. Draft Proceedings of the Sixth Advanced Functional Programming School (AFP'08), May 19-24 2008, Center Parcs "Het Heijderbos", The Netherlands, pp. 7-47. 

33. Rinus Plasmeijer, Jan Martin Jansen, Pieter Koopman, Peter Achten. Declarative Ajax and Client Side Evaluation of Workflows using iTasks. In Proceedings 10th International ACM SIGPLAN Symposium on Principles and Practice of Declarative Programming (PPDP'08). July 15-17 2008, Valencia, Spain, pp.56-66. 

34. Rinus Plasmeijer, Peter Achten. A Conference Management System based on the iData Toolkit. In Horváth, Z. and Zsók, V. Eds. Proceedings of the 18th International Symposium on Implementation and Application of Functional Languages, IFL'06, Selected Papers, September 4-6, 2006, Budapest, Hungary, Eötvös Loránd University, Faculty of Informatics, Department of Programming Languages and Compilers, Springer Verlag, LNCS 4449, pp.108-125. 

35. Rinus Plasmeijer, Peter Achten, Pieter Koopman. iTasks: Executable Specifications of Interactive Work Flow Systems for the Web. In Ramsey, N. Ed. Proceedings of the 2007 ACM SIGPLAN International Conference on Functional Programming (ICFP'07), Freiburg, Germany, October 1-3, 2007, ACM, ISBN 978-1-59593-815-2, pp. 141-152. bib 

{{< donate >}}
