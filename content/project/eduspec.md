+++
# `date -Is` -> juiste formaat
date = "2017-02-22T11:02:39+01:00"
name = "EDUSPEC"
title = "EduSpec"
summary = "Learn to interprete IR/MS/NMR spectra"
web = "http://www.eduspec.org/"
repo= ""
download_link = "http://www.eduspec.org/"
demolink = "https://demo.eduspec.org/"
expire_date = ""
forum_link = ""
contact = "t.bloemberg@science.ru.nl"
cardthumbimage = "/images/eduspec.png"
cardheaderimage = "/images/eduspec-header.png"
image = "eduspec.png"
logo_image = "eduspec.png"
sponsors = ["stitpro"]
tags = ["stitpro"]
comment = "false"


[author]
  name= "Tom Bloemberg"
  email= "t.bloemberg@science.ru.nl"
  description= "One of the authors of eduSpec"
  github= ""
  twitter= ""
  website= ""
  department = ""
  institute = ""

+++

# eduSpec

At Radboud University’s Faculty of Science, and more specifically in the Institute for Molecules and Materials (IMM) many forms of spectroscopy are very important tools that are used extensively for both routine and less conventional analyses. A number of groups actively develop new spectroscopic hardware and methodology.


Not surprisingly, learning students about spectroscopy is an important part of the programme in the Education Institute for Molecular Sciences (EIMS) that has a strong connection to the IMM. The first course in which students learn about spectroscopy mostly has a practical character and specifically aims at learning students the basics of how to interpret infrared, NMR and mass spectra.


For years, a programme called ‘iSpec4, An Introduction to Spectroscopy’ was used to let students train with recognizing specific spectral features. Although the contents of the programme were quite satisfactory, the application itself became more and more outdated in terms of graphics and – worse – in its ability to run in newer operating systems. With the advent of 64 bit processors and operating systems, the application simply did not run anymore.


After due consideration, we decided to develop our own application ‘eduSpec’ for spectroscopy tutoring. eduSpec had to be robust, extendable, open source and web-based to make it as accessible as possible to both users, educators and developers from around the world.


To achieve this, Drupal, an open source CMS was chosen as the platform on which eduSpec is built. A number of freely available Drupal Modules as well as some adapted or newly developed modules provide the necessary functionality for building an e-learning system to our (and hopefully your) liking. For displaying spectra, molecular structures and for drawing the latter, the open source JSmol applet has been integrated into a Drupal Module called ‘Molecule’.

{{< donate >}}
