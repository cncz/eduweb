+++
date = "2017-02-16T11:19:19+01:00"
name = "ASAP"
title = "ASAP"
summary = "Computer Aided Diagnosis in Pathology"
demolink = ""
repo = "https://github.com/GeertLitjens/ASAP"
forum_link = ""
link_to_docs = ""
contact = "jeroen.vanderlaak@radboudumc.nl"
cardthumbimage = "/images/pathologie-card.jpg"
cardheaderimage ="/images/pathologie-header.jpg" 
sponsors = ["stitpro"]
tags = ["stitpro"]
comment = "false"

[author]
  name = "Jeroen van der Laak"
  email = "jeroen.vanderlaak@radboudumc.nl"
  description= ""
  github= ""
  twitter= ""
  website= ""
  department = "Pathologie"
  institute = "Radboud UMC"

+++

  	
## Computer Aided Diagnosis in Pathology

### Achtergrond

Bij de diagnostiek van tumoren speelt het specialisme pathologie een belangrijke rol. De patholoog baseert zijn oordeel op nauwkeurige microscopische analyse van cellen en weefsels. De laatste jaren zijn de mogelijkheden om microscopische preparaten geheel digitaal te scannen enorm toegenomen. Digitalisering van preparaten biedt logistieke voordelen, en vormt daarnaast een eerste stap naar ondersteuning van de diagnostiek door automatische (voor)screening voor bepaalde vraagstellingen (zogenaamde computer aided diagnosis, CAD). 
In de radiologie is er al redelijk veel ervaring met het gebruik van CAD. Helaas is het niet eenvoudig om beschreven methoden en beschikbare software toe te passen op pathologiebeelden. Ten eerste is er geen uniformiteit van software in opzet en gebruikte programmeertalen. Daarnaast vereisen pathologiebeelden door hun specifieke structuur een speciale benadering: de grootte van de beelden (ruwe pixeldata 30 gigabyte of meer) vereisen een analyse in deelbeelden. Ook inhoudelijk zijn de beelden verschillend van radiologiebeelden (bijvoorbeeld aanwezigheid van kleur), wat andere herkenningsalgoritmen vereist.
 
### Doel

Doel van het huidige project is het opzetten van een open source software platform dat ontwikkelen van CAD-toepassingen in de pathologie mogelijk maakt. Daarbij wordt zoveel mogelijk gebruik gemaakt van beschikbare open source software. Naast opzetten van het platform wordt voor één vraagstelling een CAD toepassing ontwikkeld als ‘proof of concept’.
Bij vrouwen met een borsttumor wordt lymfklierweefsel weggenomen, dat vervolgens wordt onderzocht op aanwezigheid van tumorcellen (de zogenaamde poortwachterklier-procedure). Indien er tumorcellen gevonden worden heeft dit gevolgen voor de prognose van de patiënt, en worden dientengevolge eventuele vervolgbehandelingen hierop afgestemd. Automatische voorscreening van dergelijke preparaten kan leiden tot een gevoeligere methode, welke tegelijkertijd minder beslag doet op de schaarse tijd van de patholoog. Omdat een computer op een andere manier 'kijkt', is er mogelijk ook nieuwe informatie uit deze beelden te halen zodat de klinische relevantie van de procedure verder toeneemt.
 
### Resultaat

Het project heeft geleid tot een ‘open source’ digitale pathologie softwareplatform dat krachtige mogelijkheden biedt voor bekijken en annoteren van pathologiebeelden (zie Figuur 1). De software is in staat om te werken met beelden van alle veelvoorkomende scanners. Tevens kunnen analysealgoritmen eenvoudig worden geïntegreerd door aanwezigheid van een plug-in interface. Resultaten van analyses kunnen worden gevisualiseerd. De software is te downloaden op https://github.com/GeertLitjens/ASAP.

![figure 1](/images/pathologie-ASAP.jpg)

**Figuur 1** User interface van de in dit project ontwikkelde open-source software ASAP (Automated Slide Analysis Platform).

Daarnaast hebben we een algoritme ontwikkeld om volledig automatisch tumor te herkennen in lymfklieren. Het algoritme heeft een zeer goede performance, en zal in de nabije toekomst verder worden ontwikkeld en gevalideerd (zie Figuur 2). Om dit algoritme te kunnen ontwikkelen hebben we een grote set microscopische preparaten gedigitaliseerd en van annotaties voorzien (aangetekend waar eventuele tumorcellen zich bevinden). We bieden deze beelden aan andere onderzoekers aan de in de vorm van een 'grand challenge'. Dit is een soort wedstrijd waarbij deskundigen in patroonherkenning kunnen proberen het beste algoritme te ontwikkelen. De challenge kan worden gevonden op http://camelyon16.grand-challenge.org/.

![figure 2](/images/pathologie-lymphnodemeta.jpg)

**Figuur 2** Voorbeeld van tumor in lymfklier, links handmatig aangetekend (blauwe lijn) en rechts zoals herkend door het algoritme (rode vlakken).

Naast deze ‘directe’ resultaten heeft dit project geleid tot vorming van een nieuwe onderzoeksgroep op het terrein van de digitale Pathologie in het Radboud Universitair Medisch Centrum in Nijmegen.
 
### Uitvoering

Het project is uitgevoerd door de Digitale Pathologie onderzoeksgroep, op de afdeling pathologie van het Radboud Universiteit Medisch Centrum in Nijmegen, in nauwe samenwerking met de Diagnostic Image Analysis Group (DIAG) van de afdeling radiologie van dezelfde instelling.

**Projectleider** Jeroen van der Laak

**Medewerkers** Nadya Timofeeva, Geert Litjens, Meyke Hermsen

**Pathologen** Peter Bult, Ewout Schaafsma, Nils Köster
