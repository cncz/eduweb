+++
date = "2017-08-07T16:34:18+02:00"
contact = "dirkhilbers@crossbillguides.org"
name = "Ecosim"
title = "Ecosim"
summary = "Dutch ecosystem simulator"
web = "http://www.ecosim.nl"
download_link = "http://ecosim.nl/game-list"
demolink = "https://eduweb.science.ru.nl/downloads/ecosim/EcoSim_Ubuntu_64-Bit.ova.bz2"
repo= ""
forum_link = ""
cardthumbimage = "/images/ecosim.png"
cardheaderimage ="/images/ecosim-header.png" 
image = "ecosim.png"
sponsors = ["stitpro"]
tags = ["ecology", "simulator", "stitpro"]
comment = "false"

[author]
  name = "Dirk Hilbers"
  email = "dirkhilbers@crossbillguides.org"
  description= ""
  github= ""
  twitter= ""
  website= ""
  department = ""
  institute = ""
+++


# Ecosim, the educational nature management game

## Potential Users

 Bachelor/Master students Ecology, environmental science and forestry, biology curriculum HAVO and VWO

## Produced by

Foundation Crossbill Guides (specifically Dirk Hilbers, Aram Korevaar and Jaap Kamp Kreij).

## Project partners and advisory

* Wageningen University (WUR)
* Larenstein
* Environmental Education Saxion
* NIBI (Dutch Institute for Biology)
* Different high schools

Start date: 01-08-2010

Finished: 01-01-2013


## Contact

Dirk Hilbers, tel 026-3892317, email: dirkhilbers@crossbillguides.org

# Project Description

{{< youtube "tE5w_I40p6o" >}}

Ecosim is an educational game for ecology, environmental, forest and nature education for the levels of HAVO, VWO, MBO, HBO and WO. 
The game was created by the applicants, in consultation with a HAVO / VWO
school, college VHL, Saxion University of Applied Sciences (HBO environmental
science), Wageningen University and the Ministry of Agriculture. Ecosim is a
simulator of the Dutch landscape surrounding the characteristic ecosystems and
man-made landscape elements such as buildings, roads and agricultural fields.
In this landscape users can simalate nature management via commands, such as countering
the decline of rare plants and animals, restore or release species, such
as the bald eagle, or the creation of a diverse and species-rich ecosystems.
The programmed Ecosim ecosystems in the landscape are the result of a number of
biotic (eg succession, vegetation structure, the presence of dead wood) and
abiotic parameters (eg, groundwater, soil, trofiegraad, acidity). This
information takes the game to the officially recognized vegetation typology of
the SynBioSys program. SynBioSys developed by Wageningen UR / Alterra and the
Ministry of Agriculture and is the authority in the Dutch vegetation and its
relationship to the environment and soil. Ecosim being built in collaboration
with the makers of SynBioSys and will (among others) also are offered on this
program website. The Ecosimlandscape consists of thousands of small tiles that
each have their own vegetation, which is the result of the aforementioned
biotic and abiotic parameters. The tiles communicate with each other. By making
a change in the landscape (such as digging a ditch, sodding heather or create a
walking path) can change the parameters and eventually the vegetation. Applying the correct procedures, the user will succeed. When the wrong choices
are made, it will not work. To discover what action at what location in the
landscape could be successful, the user can obtain information about the
occurrence of species, hydrology, water, nutrient, etc; In other words, those
parameters which are relevant for nature and conservation.

# Demo OS-image for virtual machine

[Ubuntu demo OS-image](https://eduweb.science.ru.nl/downloads/ecosim/EcoSim_Ubuntu_64-Bit.ova.bz2) can be used to try ecosim on your own system using virtualbox or some other virtualisation tool.

{{< donate >}}
