+++
summary = "Educational formative testing tool"
title = "Physiomics"
name = "Physiomics"
contact = "Thijs.Eijsvogels@Radboudumc.nl"
link_to_docs = "https://bmcmededuc.biomedcentral.com/articles/10.1186/s12909-015-0351-0"
comment = "false"
demolink = "http://www.physiomics.eu/app"
date = "2017-04-28T14:02:02+02:00"
image = ""
cardthumbimage = "/images/physiomics-card.png"
cardheaderimage = "/images/physiomics-header.png"
repo = ""
download_link = "http://www.stitpro.nl/sites/default/files/Physiomics_OnderwijsApp.zip"
forum_link = ""
sponsors = ["stitpro"]
logo_image = ""
tags = ["stitpro", "medical", "testing" ]
# App, education, formative testing, students, learning, study performance, study behaviour. 
web = "http://www.physiomics.eu/app"

[author]
  name = "Dr. Thijs Eijsvogels"
  email = "Thijs.Eijsvogels@Radboudumc.nl"
  description = ""
  github = ""
  twitter = ""
  website = ""
  institute = "Radboud UMC"
  department = "Department of Physiology"

+++

# Physiomics, to the next level

Formative testing can increase knowledge retention but students often underuse
available opportunities. Applying modern technology to make the formative tests
more attractive for students could enhance the implementation of formative
testing as a learning tool. Interphysio aimed to develop an internet-based
formative testing application (“app”) and assessed it’s effect on study
behaviour as well as study performance of (bio)medical students. 

The “Physiomics to the next level” app is an open-source HTML-based application
and could be used on all major operating systems and devices, including cell
phones, tablets, desktops and laptops. A Dutch demonstration version of the app
can be accessed via a guest account at [www.physiomics.eu/app](http://www.physiomics.eu/app).

Students can be invited to use the app via email, providing them a personal
password and username. In the app, students have access to a tutorial course,
in which they could familiarize themselves with the use of the app, as well as
to a course specific section. Learning modules can easily be added to the app,
including movies and multiple-choice questions. Bonus questions can be unlocked
when students reach a pre-defined threshold score. All questions need to be
answered within 60 seconds and questions could only be answered once. Completed
questions remain available for review purposes at a later time. Feedback
regarding the answers to the questions in the app is provided directly by means
of a green checkmark or a red cross. In case a wrong answer is given, a pop-up
will appear referring the student to relevant pages in the course-guide and
textbook where they can search for the right answer. 

An overview of student performance is available, including the answer given to
each question, the time spent on answering a question and the number of
questions answered correctly.



