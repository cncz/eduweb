+++
date = "2016-04-13T15:51:11+02:00"
name = "VNMR Simulator"
title = "VNMR simulator"
summary = "Simulate acquisition NMR spectrum"
web = "http://www.virtualnmr.org/"
download_link = "http://eye-magination.com/customers/Radboud_Universiteit_Nijmegen/binary.zip"
src_download_link = "http://eye-magination.com/customers/Radboud_Universiteit_Nijmegen/source.zip"
contact = "Rinus.vanHerpen@ou.nl"
demolink = ""
repo= ""
forum_link = "http://www.virtualnmr.org/"
image = "IMG_8195.jpg"
cardthumbimage = "/images/IMG_8195.jpg"
cardheaderimage = "/images/IMG_8195-header.jpg"
link_to_docs = "http://eye-magination.com/customers/Radboud_Universiteit_Nijmegen/Installation%20guide%201.0.pdf"
nieuws_rss = ""
sponsors = ["stitpro"]
tags = ["nmr", "simulator", "stitpro"]
comment = "false"

[author]
	name = "Rinus van Herpen"
	email = "Rinus.vanHerpen@ou.nl"

+++

## Introduction

The VNMR simulator (referred to as VNMRsim in the following) is a program
written by Dirk van der Linden in collaboration with Marco Tessari to simulate
a virtual, high resolution NMR spectrometer. This work was started to give 1st
year molecular life science students the possibility to direct experience the
practical aspects of NMR spectroscopy.

A VNMRsim user can simulate the acquisition of an NMR spectrum of a generic
liquid sample, using a graphic interface and experimental parameters that are
identical to the ones controlling data acquisition on a real NMR spectrometer
(more precisely, a Varian Unity Inova spectrometer). Particularly, the
simulated signal is sensitive to the "missetting" of several experimental
parameters ( e.g. pulse width, recovery delay, acquisition time, receiver gain,
etc.). This makes VNMRsim a valuable tool for training NMR users (undergraduate
as well as PhD students) to data acquisition and processing.

## Development of new functionalities

A new VNMRsim graphic interface that matches the VNMR-J interface recently
installed on the NMR spectrometer of the Physical Chemistry Department is at
present being developed. The current implementation of VNMRsim is limited to 1D
pulse-acquire 1H NMR spectra. Several important spectral manipulations such as
phase correction, baseline correction, signal integration have not yet been
implemented. These functionalities will be included in the new VNMRsim
interface.

Future developments should also allow the simulation of more complex NMR
experiments (e.g. 1D NOE, T1 and T2 experiments, selective decoupling) on
protons as well as on heteronuclei (19F, 13C, 31P, 15N ,.). In addition, the
effects of magnetic field inhomogeneities and the possibility of shimming the
sample should be included. This would more or less cover all experimental
aspects that are relevant for 1st year students. The simulation of
multidimensional experiments or more exotic relaxation effects is considered as
part of a long term development of the VNMRsim software.

{{< donate >}}
