+++
date = "2017-01-03T12:12:53+02:00"
name = "SuperFLEX"
title = "SuperFLEX"
summary = "Flexible aqua modelling"
web = ""
repo = "https://github.com/gaohongkai/FLEX_interface/tree/FLEX_interface"
download_link = ""
forum_link = ""
demolink = ""
image = "hydrological.jpg"
cardthumbimage = "/images/hydrological.jpg"
cardheaderimage = "/images/hydrological-header.jpg"
contact = "h.h.g.savenije@tudelft.nl"
sponsors = ["stitpro"]
tags = ["stitpro"]
comment = "false"

[author]
  name = "Hugo Savenije"
  email = "h.h.g.savenije@tudelft.nl"
  github= ""
  twitter= ""
  website= "http://www.tudelft.nl"
  department = ""
  institute = "TU Delft"
+++

  	
# Tool for flexible model architecture in hydrological modelling

## Summary

Appropriate modelling of hydrological processes requires flexible tools which
are able to adapt to a system's dominant characteristics and to the requirements
of specific applications. However, current modelling practice is largely based
on the application of fixed model architectures based on prior perceptions. As
we shall illustrate, this hampers the progress of research and limits the
effectiveness of practical applications. Our purpose is to develop a flexible
modelling approach that can help hydrologists to understand the behaviour of a
system, and practitioners to develop tailored solutions for specific problems.
This approach will result in a toolbox where different model architectures can
be created and tested with the available data. These architectures should be
able to vary in complexity and encompass a large variety of hydrological
conditions. We expect that this tool will allow us to address fundamental
research questions for hydrological science, while at the same time it will be
a practical tool for professional applications. Possible applications include
the determination of an appropriate balance between model complexity and data
availability, the understanding of the value of data for modelling, the
investigation of appropriate ways to incorporate distributed information in the
modelling process. 

![screenshot](/images/superflex-screen.jpg)

## Documentation


This project created an interface for the FLEX hydrological model. FLEX
hydrological model allows us to choose flexible hydrological modelling
structure, based on our understanding of catchment rainfall-runoff processes.
The generic components of the FLEX model are reservoirs, lag functions and
connection elements. The flux functions describing storage-discharge
relationships, shape of lag functions, etc, are selected from a library of
functions and can be used to hypothesize and build multiple alternative model
structures. The end-users can select different generic reservoir and
corresponding constitutive functions for interception reservoir, unsaturated
reservoir, fast response reservoir and slow response reservoir. Moreover, lag
functions, splitters and multipliers can also be used as generic components to
build flexible models. FLEX model is implemented as a standalone Fortran-95
code, and is also integrated into the built-in model library in Petit Bateau.
Hence, installing Petit Bateau gives you immediate access to FLEX. This also
means we need to install Petit Bateau to run the FLEX model. The FLEX
interface, coded by C#, can be used to easily and visually generate different
model structures. After selecting the model structure in the interface, a
configuration file for Petit Bateau can be atomatically generated. Modellers
can flexibly choose their conceptual model to understand catchment
rainfall-runoff processes, as simple as playing Lego bricks.  

This project contains 5 files, including two PDF documents, two executable
software, and the source code of the interface. The two PDF documents, "An
exercise for FLEX_interface.pdf" and "Using flexible models within BATEA.pdf",
contain a couple of excercises instructing end-users to build their own model
structures by the interface, and generate the configuration files for the Petit
Bateau.  The "BATEAU_DEMO.7z" contains the Petit Bateau software. And the
"FLEX_interface.exe" is the executible file of the interface.
"FLEX_interface_SourceCode.7z" is the source code of the interface. 

{{< donate >}}
