+++
logo_image = ""
cardheaderimage = "/images/webtinq-headerimage.png"
forum_link = ""
date = "2017-02-24T12:51:53+01:00"
link_to_docs = "https://webtinq.nl/download/instruction"
download_link = ""
summary = "HTML editor for children"
demolink = ""
contact = "info@webtinq.nl"
image = ""
name = "WebTinq"
comment = "false"
sponsors = ["sidn"]
cardthumbimage = "/images/webtinq-cardimage.jpg"
tags = ["sidn", "Html", "online editor", "children" ]
web = "https://webtinq.nl"
repo = "https://github.com/louiswolf/webtinq"
title = "WebTinq"

[author]
  email = "info@webtinq.nl"
  name = "Louis Wolf"
  institute = "WebTinq"
  department = ""
  website = "https://webtinq.nl"
  twitter = ""
  github = "https://github.com/louiswolf/"
  description = ""

+++


This project enables children from 7 years and older to build and publish their
own websites in HTML. The merits of this project are not limited to teaching
children HTML and allowing them to share their work outside a teaching
environment. This contributes to making these children conscious users of the
web, by increasing their knowledge of the internet and shifting the accent from
internetconsumer to -producer. Or as they describe it themselves: programming
gives you superpowers! Innovative in this project is that the application is
specifically aimed at children to remove technical obstacles from publishing a
website online.


This project was sponsored by [SIDNfonds](https://www.sidnfonds.nl/projecten/html-websites-programmeren-en-publiceren-voor-kinderen).
