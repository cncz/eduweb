+++
date = "2017-01-03T11:32:18+02:00"
name = "MRIsim"
title = "MRIsim"
summary = "Educational simulator for MRI"
web = "http://www.mrisim.org/"
download_link = "http://www.mrisim.org/"
demolink = ""
repo= ""
forum_link = ""
link_to_docs = ""
contact = "t.oostendorp@donders.ru.nl"
cardthumbimage = "/images/mrisimlogo.png"
cardheaderimage = "/images/mrisimlogo-header.png"
image = "mrisim_poster.png"
logo_image = "mrisimlogo.png"
sponsors = ["stitpro"]
tags = ["simulator", "medical", "stitpro"]

comment = "false"

[author]
  description= "Author of MRISIM & ECGSIM"
  email= "t.oostendorp@donders.ru.nl"
  github= ""
  name= "Thom Oostendorp"
  twitter= ""
  website= "http://www.ru.nl/donders/"
  department = "DCCN"
  institute = "Radboud University"

+++


# MRIsim

Medical imaging techniques don't just
show the inside of the body as it would look like if it would be
opened; they show a
property of the tissue.
Different techniques show different properties. In order to understand
the images, or more importantly, to understand which technique to use
for which question, students need to understand what properties are
imaged by each technique.


![img123](/images/img123.png)

*The same head imaged with different
techniques*

This is particularly challenging for MR images, as
these show a weighted image of several tissue properties (in the most
basis applications: T1, T2 and hydrogen density). How these properties
are combined to a single image depends on the settings of the scanner.

MRIsim provides the student with a virtual MR scanner.
He can set the scan parameters, and the program will produce the
corresponding image for a real patient. In order to achieve this the
program uses the underlying tissue parameters for these patients,
obtained by interpolations from scans of that patient with different
scan parameters.

By varying the scan parameters and observing the
results, the student acquires an understand of the relation between the
scan parameters and the underlying tissue parameters.

![img4](/images/img4.png)

*In this example, MRIsim is used to construct images for
different values of TE. The student reads the signal intensity of
pixels for different tissues as a function of TE, and makes a plot of
the relation. In this way he discovers that the signal intensity drops
with increasing TE, but with different rates for different tissues,
discovering in this way the T2-property of the tissues.*

![img5](/images/img5.png)

![img6](/images/img6.png)
 

*In another application, MRIsim uses gradient double-pulses in
different directions. In this way the student discovers how this
technique can be used to visualize diffusion within the tissue.*

Currently MRIsim can produce spin-echo images, inverse recovery images,
diffusion weighted images and fMRI images. New imaging techniques will
be added in the future. User contributions, such as suggestions for
additions or patient cases, are welcome.

{{< donate >}}
