+++
date = "2016-11-02T15:43:19+01:00"
name = "ExperD"
title = "ExperD"
summary = "The Experiment Designer (CE) module"
web = "https://www.drupal.org/project/experd"
download_link = "https://www.drupal.org/project/experd"
repo = "http://cgit.drupalcode.org/experd/"
forum_link = "https://www.drupal.org/project/issues/experd"
demolink = ""
link_to_docs = ""
contact = "info@kryt.nl"
image = "experd.png"
cardthumbimage = "/images/experd.png"
cardheaderimage = "/images/experd.png"
sponsors = ["stitpro"]
tags = ["stitpro"]
comment = "false"

[author]
name = "Koos van der Kolk"
email = "info@kryt.nl"
  description= ""
  github= ""
  twitter= ""
  website= ""
  department = ""
  institute = ""

+++

# ExperD

Research shows that it is good to stimulate inquiry in the laboratory. Inquiry
education can require much extra work for teachers. With Experiment Designer
(ExperD) students can reap the benefits of inquiry education without hassle for
teachers. ExperD lets students design their own laboratory experiments.
Teachers stay in complete control: designs can be as closed or open-ended as
desired.

![ExperD interface](/images/experd-screenshot.png)

While students work in the lab they can use ExperD to keep an overview of the
work at hand. Students can also store their results in ExperD, divide tasks and
keep track of their progress. Teachers can monitor students’ progress in real
time. 

A commercial variant of this product is also available, called [LabBuddy](http://www.labbuddy.net)

Koos van der Kolk

{{< donate >}}
