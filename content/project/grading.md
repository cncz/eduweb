+++
# `date -Is` -> juiste formaat
date = "2017-07-15T10:39:33+02:00"
name = "Digigrading"
title = "TAO digital exam"
summary = "grading and review application"
web = ""
download_link = ""
demolink = ""
repo= ""
forum_link = ""
link_to_docs = ""
contact = "f.melssen@science.ru.nl"

cardheaderimage = "/images/default-header.png"
cardthumbimage = "/images/default-card.png"
image = ""
logo_image = ""

sponsors = ["fnwi"]
tags = ["fnwi"]
comment = "false"

[author]
  name = "Fred Melssen"
  email = "f.melssen@science.ru.nl"
  description= ""
  github= ""
  twitter= ""
  website= ""
  department = "Faculty of Science"
  institute = "Radboud University"

+++

  	
# TAO digital exam – grading and review application


here some screenshots:

![screenshot1](/images/TAO1.jpg)

![screenshot2](/images/TAO2.jpg)

![screenshot3](/images/TAO3.jpg)

![screenshot4](/images/TAO4.jpg)
