+++
# `date -Is` -> juiste formaat
date = "2017-11-24T15:57:58+01:00"
name = "E-neuro"
title = "RUDiag + E-neuro"
summary = "Design of Simulated Research Projects"
web = "https://rudiag.science.ru.nl/index.html#/test/281/1718%3A+Simulated+Research+Project+2+-+for+MCN"
download_link = ""
demolink = ""
repo= "https://github.com/Simplendi/RUDiag2"
forum_link = ""
link_to_docs = ""
contact = "g.martens@ncmls.ru.nl"

cardheaderimage = "/images/eneuro-header.png"
cardthumbimage = "/images/eneuro-card.png"
image = ""
logo_image = ""

sponsors = ["stitpro"]
tags = ["stitpro", "simulator"]
comment = "false"

[author]
  name = "Tim Cooijmans"
  email = "t.cooijmans@simplendi.com"
  description= ""
  github= "https://github.com/Simplendi/"
  twitter= ""
  website= ""
  department = ""
  institute = "Simplendi"

+++

# E-neuro: Design of Simulated Research Projects

E-neuro is flexible software to design simulated research projects. It concerns
a user-friendly (for both teacher and student), interactive, robust, efficient
(i.e., requires limited assistance from teaching assistants) and broadly
applicable (including options with choices in answers and paths as follow-ups
of the answers) software with up-to-date graphical possibilities (e.g.,
possibility of including movies), open source (with central collection of
answers and possibilities for follow-up communication with students) and web
based. Furthermore, the new software is cost effective because during the
simulated research projects the students are working mostly independent (all
information and explanations are in principle provided by the open-source,
web-based project contents) and thus a relatively low number of student
assistants are required.



The simulated research projects consider a number of technical approaches and
research strategies. The students learn independent, stepwise and critical
thinking about research techniques and approaches, and in this way also get
familiar with the terminology used by experienced researchers. The simulated
projects are designed to be interactive and are not intended for the students
to just memorize concepts without understanding how experiments are done and
why they are done that way. The students should develop an independence in
their learning skills.  The goal of the simulated projects is to teach the
students how to judge a
research problem (experimental set-up to be used, inventory and choice of
techniques/approaches, interpretation of the results obtained) and how to
develop a research strategy, and to make them familiar with research
terminology. The students focus on research strategies and techniques involved;
theoretical aspects are less important. The experiments are thus performed “on
the screen”, i.e. not with “wet experiments”, and the students are supported
and directed on the basis of information delivered to them via the course-URL.
At every stage in the simulated research project, the students have to decide
which strategy is the most appropriate one and they have to stepwise answer a
number of questions. When the students have made a decision on the strategy or
have come to an answer to a particular question, they write down this
decision/answer in a file (text box in the page) before they can continue to
the next page. On the next page, the students receive the answer to the
previous question (often with the results of this part of the research,
including a figure/table) followed by information concerning a new strategy or
a new step, and a further question (including concerning the interpretation of
the results from the figure/table), etc. See below for an example of an E-neuro
learning path.




## Functional description of E-neuro: Adaptive learning path.
![Functional description of E-neuro: Adaptive learning path.](/images/eneuro-funcdesc.png)




## Functional units

* Start (yellow circle with arrow)
* Transport (black arrows)
* Theoretical units (blue blocks)
* Deepening units (grey blocks)
* Deepening links (blue arrows)
* Assignments (orange blocks)
* Route option (green block with splitting arrows)
* Back to Route option (green block with curved arrow)
* Termination (yellow circle with check mark)


# E-neuro requirements and main use

The idea to extend RUDiag with features required for E-neuro originates from
Gerard Martens (g.martens at science.ru.nl) of the Molecular Animal Physiology
department, Donders Centre for Neuroscience (DCN) at the Faculty of Science,
Radboud University.
