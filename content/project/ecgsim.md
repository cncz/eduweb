+++
date = "2016-04-12T11:24:18+02:00"
name = "ECGSIM"
title = "ecgsim"
summary = "Simuleer ecgs in software"
download_link = "http://www.ecgsim.org/"
web = "http://www.ecgsim.org/"
demolink = ""
repo= ""
expire_date = ""
forum_link = "http://www.ecgsim.org/"
contact = "peter.m.vandam@xs4all.nl"
cardthumbimage = "/images/ecgsimlogo.png"
cardheaderimage ="/images/ecgsimlogo-header.png" 
image = "ecgsim.png"
logo_image = "ecgsimlogo.png"

sponsors = ["stitpro"]
tags = ["stitpro", "simulator", "medical"]
comment = "false"

[author]
  name = "Thom Oostendorp"
  email = "t.oostendorp@donders.ru.nl"
  description= ""
  github= ""
  twitter= ""
  website= ""
  department = "DCCN"
  institute = "Radboud University"
+++


 	
# ECG Simulation

 ECGSIM is an interactive simulation program that enables one to study the relationship between the electric activity of the myocardium and the resulting potentials on the thorax: PQRST wave forms as well as body surface potential maps.
Education

One of the purposes of ECGSIM is to serve as a teaching tool for students of the basic aspects of electrocardiography. In a typical setting students use ECGSIM in a lab class environment. Following instructions, they change the electric activity of the heart and observe the effect on the ECG. Example instructions may be downloaded from the download area.
Research

ECGSIM is also used as a research tool for those that are interested to test any hypothesis they may have regarding the manifestation of cardiac malfunctioning in the electrocardiographic wave forms on the thorax.

Please note: Any diagnostic application is only indirect; ECGSIM provides a forward simulation and does not solve the inverse problem.
Basis usage

ECGSIM's main windows consists of several panes. One of them displays the heart, together with a map of the depolarization times, repolarization times or transmembrane potential amplitude at the heart. The user may change these parameters, and observe in the other panes the effect of these changes on the ECG and the potential at the body surface. 

{{< donateparm buttonid="7X37NXMV3KZS4" text="This project is sponsored by">}}

