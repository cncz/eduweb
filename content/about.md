---
title: "About Eduweb"
description: "Eduweb is an effort to support and keep available Free and open Source projects for Education"
date: "2017-05-04T14:58:04+02:00"
type: "about"
layout: "single"
blurb: "Free software for Science and Education"
photo: "images/icto-avatar.gif"
cardheaderimage: "" #optional: default solid color if unset
cardbackground: "#263238" #optional: card background color; only shows when no image specified
---

## Eduweb

Eduweb is a product based on an idea of [StITPro](http://www.stitpro.nl/) and the [Faculty of Science](http://www.ru.nl/science) to promote and facilitate open source educational software projects and thus ensure continuity by providing community forming tools, like centralised repositories, download and discussion facilities. Also, if a product has no properly maintained website, we can easily create one that looks good and works on all platforms.

## [C&CZ](https://wiki.science.ru.nl/cncz/index.php?title=Hoofdpagina&setlang=en)

The IT support department of the [Faculty of Science](http://www.ru.nl/science) plays a central role in the creation and operation of the Eduweb project. We have the hosting facilities and technical expertise to support the goals of the Eduweb project.

## Open Source

One of the reasons why this site lists only open source (or **Free** as in freedom) software is that especially in educational use, being able to see how tools work is useful. Also there's always a risk with software that it may fall into disuse or the developers are working on other/newer projects. Software needs frequent "love" to stay current and runnable on modern systems. Free software, while no magic solution, is a way for software to remain usable without running into legal issues or dead software (unmaintained, unsupported, old).

Another advantage of open source software is opportunities for collaboration. [Here's a good article](https://www.oreilly.com/ideas/open-source-mistakes-for-enterprise-newcomers) about advantages and common misunderstandings about open source software.

## Contact

For questions about Eduweb, please send e-mail to *icto@science.ru.nl*

## Disclaimer images

Some images used in this site are copyrighted material, but we are unable to find the sources. If you find copyrighted material on this site for which there is no legal agreement, please contact us so we can take proper action.

## Site Manual

The use of this site is mostly as a starting point for several otherwise unrelated projects, in order to provide continuity and findability. 

The projects overview shows a very short tagline, when available, icons with links to the repository and/or project web-site. When you click on the card or the more-info link, a short introduction to the project is provided, as well as more information about the author. The icons for the (github) repository and website are shown here as well.

## Completeness of information per project

In order to track whether we have enough relevant information about each project, there's a [page that shows what we currently know about the projects](/meta/) listed here.

