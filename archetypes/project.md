+++
# `date -Is` -> juiste formaat
date = "2017-02-24T12:51:53+01:00"
name = ""
title = "Project Title"
summary = "The Project Summary"
web = "http://link.to.site/"
download_link = ""
demolink = ""
repo= ""
forum_link = ""
link_to_docs = ""
contact = ""
cardheaderimage = "/images/default-header.png"
cardthumbimage = "/images/default-card.png"
image = ""
logo_image = ""

sponsors = ["stitpro"]
tags = ["stitpro"]
comment = "false"

[author]
  name = "Project Person"
  email = "pp@projectdomain.nl"
  description= ""
  github= ""
  twitter= ""
  website= ""
  department = "PP Department"
  institute = "Radboud University"

+++

  	
# Title

