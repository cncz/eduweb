#!/bin/bash

# deploy hugo sites

# eventueel vervangen door config file als argument of stdin?
hsitescfg=$(mktemp)
cat <<EOF >$hsitescfg
#repo branch server path
/scratch/tmp/repo1	master	lilo	tmp/repo1
/scratch/tmp/repo2	test	lilo	tmp/repo2
git@gitlab.science.ru.nl:cncz/etalage.git	master	lilo	tmp/repo3
git@gitlab.science.ru.nl:cncz/etalage.git	test	lilo	tmp/repo4
EOF

if ! [ -x $(type -p git) ]; then
	echo "git not found" >&2
	exit 1
fi

if ! [ -x $(type -p hugo) ]; then
	echo "hugo not found" >&2
	exit 1
fi

deploy () {
	local repo="$1"
	local branch="$2"
	local server="$3"
	local path="$4"

	local dir="$PWD"
	localrepo=$(mktemp -d)
	#clone specific branch into local repo
	git clone -b "$branch" $repo  $localrepo
	cd $localrepo/hugo-etalage
	# generate static pages in public/
	hugo
	scp -r public/* $server:$path

	# done, clean up...
	cd "$dir"
	rm -rf $localrepo
}


cat $hsitescfg | grep -v '^[ ]*#' | while read repo br server path
do
	deploy "$repo" "$br" "$server" "$path"
done

rm -f $hsitescfg

