StITPro/RU-Science Eduweb
=========================

The staticgen website is made using [Hugo](https://gohugo.io/)
The [Eduweb theme](https://gitlab.science.ru.nl/cncz/eduweb-theme.git) is (based on) [HugoMDL](http://themes.gohugo.io/hugo-mdl/)

Hugo is a fast website compiler to create static websites from Markdown files.

The advantage of a staticgen website is to create a website without any traction for malware injection, thereby reducing the security overhead of running the website.

Deploying the Eduweb website
----------------------------

_Requirements_
1. Clone of this repository in directory `X`: `git clone https://gitlab.science.ru.nl/cncz/eduweb.git X`
1. `mkdir X/themes && cd X/themes`
2. Clone of Eduweb theme in `X/themes/HugoMDL`: `git clone https://gitlab.science.ru.nl/cncz/eduweb-theme.git HugoMDL`
3. A working hugo (recommended version: latest, but no older than *0.18*)

_View site on local machine_
1. modify `start-hugo` script to use the correct path to hugo's single executable
2. run `start-hugo` from directory `X` like: `./start-hugo`
3. connect to http://localhost:1313/ (NB: use firefox or chrome/chromium to browse) 

_Deploy site to webserver_
1. modify `start-hugo` script to use the correct path to hugo's single executable
2. modify `config.toml` to change baseurl to the correct value (must be a URL without a path) example: http://x.example.com/
2. run `start-hugo` from directory `X` like: `./start-hugo gen`
3. copy the entire tree of files/directories from `./public/` to a webserver.
4. connect to http://x.example.com/ (NB: use firefox or chrome/chromium to browse) 

